<?php

/** This will ensure these are only loaded on Lando */
if (getenv('LANDO_INFO')) {
	/**  Parse the LANDO INFO  */
	$lando_info = json_decode(getenv('LANDO_INFO'));

	/** Get the database config */
	$database_config = $lando_info->database;
	/** The name of the database for WordPress */
	define('DB_NAME', $database_config->creds->database);
	/** MySQL database username */
	define('DB_USER', $database_config->creds->user);
	/** MySQL database password */
	define('DB_PASSWORD', $database_config->creds->password);
	/** MySQL hostname */
	define('DB_HOST', $database_config->internal_connection->host);

	/** URL routing (Optional, may not be necessary) */
	define('WP_HOME', $lando_info->appserver->urls[0]);
	define('WP_SITEURL', $lando_info->appserver->urls[0]);
}

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'wordpress');

/** MySQL hostname */
define('DB_HOST', 'database');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'FRklg|<i%pvbpIa{=QM|lH>1)n-Nvqk^.l^ROkS-)AU0`5?vJcH^:=1B-t$7Tw[/');
define('SECURE_AUTH_KEY',  'z,%-O]gW!{a3hjIXetS@B|=]@o!ugY1+Oa}h+[1M.(/fqZFTWvOH-~>}BRk%W|&|');
define('LOGGED_IN_KEY',    '%2? `%l,4,d_t?+pI}7tF#u$&yT;HD2M)}>!JSBR{G}SHeEq9(22z_|tm:?qgnc;');
define('NONCE_KEY',        'h^:>_8+[J]T+gIY7]UDo$t,s#wqKDh6*25-PEl! n+Zd~^Z =-l_!z`t 8s`vuHW');
define('AUTH_SALT',        'S+}(Q::0DhX+x7LHi+o&)4rQz.GmcE-H:NE+P@3?+#sL%)e8WzIkLz=g~fPk6#Xq');
define('SECURE_AUTH_SALT', 'At<%+~jH7/>;yv%vX;8VBU1KD=B|Qj-$hP!}o~n8%{u1dW,aiBoPLGv]K#<6E.2N');
define('LOGGED_IN_SALT',   '$0V;nr=39a9L|-6=HQ<t/h>jU};^]wom]4-!R#`y3vNVo-/^MU|-V_9wB/<N- Wx');
define('NONCE_SALT',       '8d~=8q?(0D(2VOThJLir=@+tDF8!5MH)G{w9H69bq1HDL-u%!6}#]>atL)Wr%5OK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', __DIR__ . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
